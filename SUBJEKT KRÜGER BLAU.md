# Subjekt KRÜGER BLAU

## CODEWÖRDER: KRÜGER BLAU, FALL ROLLENDER DACHSTUHL

## Beschreibung

Bei KRÜGER BLAU scheint es sich um eine Infektion durch eine bisher unbekannte Spezies von Zygomycota zu handeln. Die Vermutung liegt nahe, dass die Spezies unter dem Einfluss extradimensionaler Entitäten mutiert wurde.

Bei dem Fungus konnten bisher zwei dedizierte und separate trophischen Stadien festgestellt werden; die kolonisierende und die reproduktive Phase. Die kolonisierenden Phase wird primär von der Bildung von Mycelium begleitet. Das Mycelium hat die Eigenschaft, jegliches organische Material bis zu mehrere Millimeter tief zu durchdringen und durch die Absonderung von Enzymen zu metabolisieren. Gemäss Einsatzbericht FALL ROLLENDER DACHSTUHL (Anhang B) ist KRÜGER BLAU in der Lage, die menschliche Haut innert weniger Minuten zu durchdringen. Mit Hilfe verschiedener organischer Adhesive (siehe Anhang A) bindet sich das Mycelium an organisches Material, zudem wird die Beute durch den Ausstoss von Sedativen von der Flucht abgehalten. Besonders erwähnenswert ist die Abgabe von 2-Diethylamino-acetamid, weitere Untersuchungen sind hinsichtlich der bei FALL ROLLENDER DACHSTUHL durch AGENT EHRLICH zum ersten Mal beschriebenen Suggestionsphänomenen (Siehe Initalbericht FALL ROLLENDER DACHSTUHL, Anhang B) notwendig.

In der reproduktiven Phase wird sämtliche gewonnene Energie in die Produktion von Fruchtkörpern investiert. Die rund fünf Millimeter durchmessenden Fruchtkörper sitzen auf bis zu zwanzig Millimetern langen Apophysen. Wie bei Zygomycota üblich, besteht das Sporangium aus einem sich auf mehrere tausend einzelne Sporenträger aufgeteilten Plasmodium. Die Sprorangien sitzen in einem dichten Geflecht von stark adhesiven Mycelien. Bei Reife und unterstützt durch physischen Kontakt werden die Sporen freigegeben. Diese können sich mehrere Stunden lang in der Luft halten und setzen sich auf jeglicher Oberfläche ab. Gelangen die Sporen in die Lungen von Wirbeltieren, so ist mit einer Infektion in unter 30 Minuten zu rechnen. In diesem Falle (Siehe Anamnese und Vivisektion Subjekt FEHLENDER BLEISTIFT, Anhang C) kann eine Kolonisierung des zentralen Nervensystemes in unter 24 Stunden geschehen. Die Kolonisierung wird begleitet von einer weitreichenden Encephalopathie.


## Spezielle Aufbewahrungsmassnahmen

Proben von KRÜGER BLAU sollten in hermetisch versiegelten Keramik- oder Glasbehältern (keinesfalls Metall, siehe Anhang B) gelagert werden. Um die Aktivität von KRÜGER BLAU auf ein Minimum zu reduzieren, sollten die Proben bei 4,25°C gelagert werden. 

Personal, welches mit der Handhabung von KRÜGER BLAU beauftragt ist, muss in jedem Fall durch eine BSS/3-Barriere (mobil oder stationär) vor jeglichem direkten Kontakt mit KRÜGER BLAU geschützt werden. Proben von KRÜGER BLAU, welche aus der Sicherheitsaufbewahrung entfernt werden, oder Material, welches in Kontakt mit KRÜGER BLAU gekommen ist, müssen gemäss Protokoll BIOPA/BRD1973R1 ("Empfang, Bearbeitung, Verwahrung, Lagerung und Entsorgung von biologischem Gefahrenmaterial") unter Einsatz eines Hochdruckveraschers zerstört werden.


## Beiliegende Dokumente:

 - A: Initialkontakt FALL ROLLENDER DACHSTUHL, AGENT EHRLICH
 - B: Abschlussbericht FALL ROLLENDER DACHSTUHL, AGENT SEITENSTRASSE
 - C: Anamnese und Vivisektion Subjekt FEHLENDER BLEISTIFT, AGENT ROLLMATTE
 - D: Ritualmagische Analyse von KRÜGER BLAU, AGENT SEITENSTRASSE
